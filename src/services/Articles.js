import axios from 'axios'
import { articlesEndpoint } from '../constants/endpoints'

const getArticles = async () => {
    return await axios.get(articlesEndpoint)
}

const setDeleted = async(id) => {
    return await axios.put(`${articlesEndpoint}/${id}`)
}

export {getArticles, setDeleted}