import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import ErrorBoundary from '../views/ErrorBoundary'
import Articles from '../containers/Articles'
import NavBar from '../components/NavBar'

/* Routes */
/* Nav, autentification, error bundle */
function Routes() {
  return (
    <ErrorBoundary>
      <BrowserRouter>
      <div className="App">
          <NavBar name="Node.js on Hacker News" />
        </div>
        <Route exact path="/" component={Articles} />
      </BrowserRouter>
    </ErrorBoundary>
  )
}

export default Routes
