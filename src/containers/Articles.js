import React, { useEffect, useState } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import Snackbar from '../components/Snackbar'
import {withRouter} from 'react-router-dom'
import moment from 'moment'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import Loading from '../components/Loading';
import { getArticles, setDeleted } from '../services/Articles'
import '../scss/articles.scss'

import { textError, textLoading, textNoData, textNoAvalaible,textErrorDelete, textDeleteSuccess } from '../constants/messages'

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#333'
        }
    },
    overrides: {
        MuiListItemText: {
            root: {
                fontSize:"13pt"
            }
        },
        MuiListItem:{
            root: {
                "&:hover": {
                  backgroundColor: '#fafafa !important'
                },
                width: '97%'
            }
        },
        MuiButtonBase:{
            root: {
                borderBottom: '1px solid #ccc',
            }
        },
        MuiTypography:{
                caption:{
                    color: '#999'
                },
                body1:{
                    color: '#333'
                }
            }
        
    },
});

function Articles(){
    const [articles, setArticles] = useState([]),
     [isLoading, setLoading] = useState(0),
     [showDelete, setShowDelete] = useState({show: false, id: ''}),
     [message, setMessage] = useState(''),
     [snackbar, setSnackbar] = useState({
        visible: false,
        message: '',
        type: 'success'
    })

    const goToDetail = (url) => () => {
        if(url)
            window.open(url, "_blank")
        else
        setSnackbar({                    
            visible: true, 
            message: textNoAvalaible, 
            type:  'warning'
        })
    }

    const goToDelete = (id) => async() => {
        const isDeleted = await setDeleted(id)
        setSnackbar({                    
            visible: true, 
            message: isDeleted.status === 200 ? textDeleteSuccess: textErrorDelete, 
            type:  isDeleted.status === 200 ? 'success': 'warning'
        })
        await onGetArticles()
    }

    const dateFormat = (date) => {
        const current = moment().format('DDMMYYYY')
        const dateFormat = moment(date).format('DDMMYYYY')
        
        if(dateFormat === current){
            return moment(date).format('hh:mm A')
        } else if((current - dateFormat) === 1000000)
            return 'yesterday'
        else 
            return moment(date).format('MMMM DD')
    }

    const handleCloseSnackbar = () => {
        setSnackbar({
                ...snackbar,
                visible: false
            }
        )
    }

    const onGetArticles = async () => {
        setLoading(true)
        setMessage(textLoading)
        try{
            let { data } = await getArticles()
            data = data.filter((i) => i.story_title || i.title)
            setArticles(data)
            if (data && data.length > 0) {
                setMessage('')
            } else {
                setMessage(textNoData)
            }
        } catch(e){
            setMessage(textError)
            setArticles([])
        }
        
        setLoading(false)
   }

    useEffect(() => {
        onGetArticles()
      }, [])
    
      return(
        <MuiThemeProvider theme={theme}>
            <div className="playlist">
                <List component="nav" aria-label="main mailbox folders" id="list" onMouseLeave={() => setShowDelete({show: false, id: ''})}>
                      {articles.map((i, index) => (
                        <ListItem key={index} role={undefined} dense button onClick={goToDetail(i.url || i.story_url)} onMouseEnter={() => setShowDelete({show: true, id: i._id})}>
                            <ListItemText id={i.created_at} primary={
                                <Typography variant="body1" style={{ width: '70%' }}>
                                    {i.title || i.story_title} 
                                        <Typography variant="caption" style={{ marginLeft: '5px' }}>
                                        - {i.author} - 
                                        </Typography></Typography>} />
                            <ListItemText className="dateText" primary={dateFormat(i.created_at)}/>
                            {(showDelete.show && showDelete.id === i._id) && <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="delete" onClick={goToDelete(i._id)}>
                                    <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>}
                        </ListItem>
                    ))}
                </List>
                {(isLoading || message) && <Loading isLoading={isLoading} message={message}/>}
                <Snackbar open={snackbar.visible} message={snackbar.message} type={snackbar.type} handleClose={handleCloseSnackbar}></Snackbar>
            </div>
        </MuiThemeProvider>
      )
}

export default withRouter(Articles)