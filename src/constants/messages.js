const textError = "Ups! Ocurrio un error y no se puede mostrar la lista.",
      textLoading ="Cargando...", 
      textNoData="No se encontraron datos",
      textNoAvalaible="Ups! Este post no tiene url disponible",
      textErrorDelete="Ups! El post no fue eliminado.",
      textDeleteSuccess="No volveras a ver ese post!!!"

export {textError, textLoading, textNoData, textNoAvalaible, textErrorDelete, textDeleteSuccess}