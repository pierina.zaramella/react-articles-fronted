import 'react-table/react-table.css'
import React, {Component} from 'react'
import ButtonContained from '../components/Button';
import  ReactTable from "react-table";
import  getLogs  from "../services/logs";
import debounce from 'lodash/debounce';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#0371CE'
    }
  }
});

export default class Logs extends Component{
  
    state = {
        data: [],
        loading: false,
        pages: 0,
        page: 1, 
        pageSize: 10, 
        sorted: '', 
        filtered: ''
     };
    
    table = React.createRef();
    
    onRefreshData = () => {
      const state = {page: 1, pageSize: 10, sorted: [], filtered:[] }
      this.table.current.fireFetchData(state)
    }

    onFetchData = (state) => {
        this.setState({loading: true});
        getLogs(state.page+1, state.pageSize, state.sorted, state.filtered, (res) => {
          this.setState({
                data: res.data.docs,
                pages: res.data.totalPages,
                loading: false
          })
        }).catch(e=> {
        this.setState({
          data: [],
          pages: 0,
          loading: false
        })
      })
    }
    render(){
        const { data, pages } = this.state;
          return (
            <MuiThemeProvider theme={theme}>
            <div style={{display: 'flex', justifyContent: 'flex-end'}}>
              <ButtonContained onClick={this.onRefreshData} name="Recargar datos" color="primary" />
            </div>
            <div style={{padding:'0 10px'}}>
            <ReactTable
                  ref={this.table}
                  data={data}
                  pages={pages}
                  previousText= "Anterior"
                  nextText = "Siguiente"
                  loadingText = "Cargando..."
                  noDataText = "No hay registros"
                  pageText = "Página"
                  ofText = "de"
                  rowsText = "filas"
                  columns={[
                        {
                          Header: "Id Producto",
                          accessor: "productId"
                        },
                        {
                          Header: "Descripción",
                          accessor: "name"
                        },
                        {
                          Header: "Sku Sodimac",
                          accessor: "skuCl"
                        },
                        {
                          Header: "Sku Proveedor",
                          accessor: "skuVin"
                        },
                        {
                          Header: "Stock Anterior",
                          accessor: "stockActual"
                        },
                        {
                          Header: "Stock",
                          accessor: "stockUpdated"
                        },
                        {
                          Header: "Unidad",
                          accessor: "unity"
                        },
                        {
                          Header: "Id transacción",
                          accessor: "trasactionId",
                          filterMethod: (filter, row) =>{
                            row[filter.id].startsWith(filter.value)
                          }   
                        },
                        {
                          Header: "Mensaje",
                          accessor: "message"
                        }
                        
                      ]}
                resized={[{ // the current resized column model
                    "id": "productId",
                    "value": 100
                  },{ // the current resized column model
                    "id": "name",
                    "value": 320
                },{ // the current resized column model
                  "id": "skuCl",
                  "value": 150
                },{ // the current resized column model
                  "id": "skuVin",
                  "value": 150
                },{ // the current resized column model
                  "id": "stockUpdated",
                  "value": 140
                },{ // the current resized column model
                  "id": "stockActual",
                  "value": 130
                },{ // the current resized column model
                  "id": "unity",
                  "value": 100
                },{ // the current resized column model
                  "id": "trasactionId",
                  "value": 150
                },{ // the current resized column model
                  "id": "message",
                  "value": 305
                }]}
                defaultPageSize={10}
                className="-striped -highlight"
                loading={this.state.loading}
                showPagination={true}
                showPaginationTop={false}
                showPaginationBottom={true}
                filterable
                resizable
                pageSizeOptions={[5, 10, 20, 25, 50, 100]}
                manual // this would indicate that server side pagination has been enabled 
                onFetchData={debounce(this.onFetchData, 1000)}
          />
          </div>
          </MuiThemeProvider> 
    )}
}

