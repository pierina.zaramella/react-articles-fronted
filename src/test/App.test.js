import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


/*it('should have proper props for email field', () => {
    expect(container.find('input[type="email"]').props()).toEqual({
      className: 'mx-auto my-2',
      onBlur: expect.any(Function),
      placeholder: 'email',
      type: 'email',
    });
  });
  describe('LoginMethods()', () => {
  it('isEmailValid should return false if email is invalid', () => {
    expect(LoginMethods().isEmailValid('notvalidemail')).toBeFalsy();
    expect(LoginMethods().isEmailValid('notvalidemail.aswell')).toBeFalsy();
  });
  it('isEmailValid should return false if email is valid', () => {
    expect(LoginMethods().isEmailValid('validemail@gmail.com')).toBeTruthy();
  });
  /* Similar for isPasswordValid and areFormFieldsValid */
});
  
  */