import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography  from '@material-ui/core/Typography'
import {makeStyles} from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#fff'
    }
  },
  overrides: {
      MuiToolbar:{
        root:{
          flexDirection: 'column',
          alignItems:'start',
          padding: "25px 0",
          color:'#ccc'
        }
      }
    }
  });

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    },
    navBar: {
      padding: '10px 0',
      borderBottom: '1px solid #eee',
      boxShadow: 'none',
      backgroundColor: '#333'
    },
    containerDiv: {
      textAlign: 'center'
    }
  }
  ));

  function NavBar({name, history}) {
    const classes = useStyles()

    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <AppBar className={classes.navBar} position="static" color="primary">  
            <Toolbar>         
                <Typography variant="h3" gutterBottom>
                  HN FEED
                </Typography>
                <Typography variant="h6">
                  We {'<'} hacker news!
                </Typography>
            </Toolbar> 
          </AppBar>
          
        </div>
      </MuiThemeProvider>
    )
}

export default withRouter(NavBar)

