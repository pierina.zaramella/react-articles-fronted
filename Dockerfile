FROM node:latest as build

### set working directory
FROM node:alpine
RUN mkdir /var/www
WORKDIR /var/www

### copy modules
COPY package.json .
COPY src src
COPY public public

### add `/var/www/node_modules/.bin` to $PATH
ENV PATH /var/www/node_modules/.bin:$PATH

### install dependencies
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent

### start app
CMD ["npm", "start"]